package com.example.aufgabe_stadtwerke;

import org.springframework.core.MethodParameter;
import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.standard.ValueProvider;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MyValuesProvider implements ValueProvider {

    @Override
    public List<CompletionProposal> complete(CompletionContext completionContext) {
        List<CompletionProposal> result = new ArrayList<>();
        List<String> knownThings = new ArrayList<>(Arrays.asList("education", "recreational", "social", "diy", "charity", "cooking", "relaxation", "music", "busywork"));

        String userInput = completionContext.currentWordUpToCursor();
        knownThings.stream()
                .filter(t -> t.startsWith(userInput))
                .forEach(t->result.add(new CompletionProposal(t)));
        return result;

    }

}
