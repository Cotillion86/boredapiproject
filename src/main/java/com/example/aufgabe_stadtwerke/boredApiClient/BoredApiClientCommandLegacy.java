package com.example.aufgabe_stadtwerke.boredApiClient;

import com.example.aufgabe_stadtwerke.MyValuesProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.web.client.RestTemplate;

import static org.springframework.shell.standard.ShellOption.NULL;

@ShellComponent(value="Bored commands with autocomplete")
public class BoredApiClientCommandLegacy extends BoredApiClient {

    @Autowired
    public BoredApiClientCommandLegacy(RestTemplate restTemplate) {
        super(restTemplate);
    }

    @ShellMethod(key="receive", value="Gets an activity.")
    public String getActivityByType(
            @ShellOption(value = { "--type", "-t" }, defaultValue=NULL, help="Type of the activity(with autocomplete): [education, recreational, social, diy, charity, cooking, relaxation, music, busywork]", valueProvider = MyValuesProvider.class) String type)
    {
        StringBuilder requestUrlBuilder = new StringBuilder(API_URL);
        requestUrlBuilder = appendParameter(requestUrlBuilder, "type", type);
        String request = requestUrlBuilder.toString();
        System.out.println(request);
        return sendRequest(request);


    }
}
