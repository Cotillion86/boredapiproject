package com.example.aufgabe_stadtwerke.boredApiClient;


import com.example.aufgabe_stadtwerke.boredApiClient.BoredApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.command.annotation.Command;
import org.springframework.shell.command.annotation.Option;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.web.client.RestTemplate;


@Command
@ShellComponent
public class BoredApiClientCommand extends BoredApiClient {

    @Autowired
    public BoredApiClientCommand(RestTemplate restTemplate) {
        super(restTemplate);
    }


    @Command(command = "get", group="Bored commands", description = "Gets an activity.")
    public String getActivity(@Option(longNames = "type", shortNames = 't', description=" Type of the activity: [education, recreational, social, diy, charity, cooking, relaxation, music, busywork]") String type,
                              @Option(longNames = "participants", shortNames = 'p', description="The number of people that this activity could involve: [0 to n]") String participants,
                              @Option(longNames = "accessibility", shortNames = 'a', description="A factor describing how possible an event is to do with zero being the most accessible: [0.0 to 1.0]") String accessibility,
                              @Option(longNames = "cost", shortNames = 'c', description="A factor describing the cost of the event with zero being free: [0.0 to 1.0]") String cost,
                              @Option(longNames = "key", shortNames = 'k', description="A unique numeric id: [1000000 to 9999999]") String key
                              ) {

        StringBuilder requestUrlBuilder = new StringBuilder(API_URL);

        requestUrlBuilder = appendParameter(requestUrlBuilder, "type", type);
        requestUrlBuilder = appendParameter(requestUrlBuilder, "participants", participants);
        requestUrlBuilder = appendParameter(requestUrlBuilder, "accessibility", accessibility);
        requestUrlBuilder = appendParameter(requestUrlBuilder, "price", cost);
        requestUrlBuilder = appendParameter(requestUrlBuilder, "key", key);

        String request = requestUrlBuilder.toString();
        System.out.println(request);

        return sendRequest(request);
    }

}
