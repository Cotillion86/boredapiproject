package com.example.aufgabe_stadtwerke.boredApiClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public abstract class BoredApiClient {

    String API_URL = "http://www.boredapi.com/api/activity";
    RestTemplate restTemplate;
    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public BoredApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    protected String sendRequest(String request) {
        ResponseEntity<String> response = restTemplate.getForEntity(request, String.class);

        try {
            JsonNode jsonNode = objectMapper.readTree(response.getBody());
            if (jsonNode.get("activity") != null) {
                return jsonNode.get("activity").asText();
            } else {
                return jsonNode.get("error").asText();
            }
        } catch (Exception err) {
            return "Ungültige URL";
        }
    }

    protected StringBuilder appendParameter(StringBuilder requestUrlBuilder, String paramName, String paramValue) {
        if (paramValue != null) {
            requestUrlBuilder.append(requestUrlBuilder.toString().contains("?") ? "&" : "?")
                    .append(paramName).append("=").append(paramValue);
        }
        return requestUrlBuilder;
    }
}

