package com.example.aufgabe_stadtwerke;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.shell.command.annotation.CommandScan;
import org.springframework.shell.command.annotation.EnableCommand;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@CommandScan
public class AufgabeStadtwerkeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AufgabeStadtwerkeApplication.class, args);
	}

}
