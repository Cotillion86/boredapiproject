package com.example.aufgabe_stadtwerke;

import com.example.aufgabe_stadtwerke.boredApiClient.BoredApiClientCommand;
import com.example.aufgabe_stadtwerke.boredApiClient.BoredApiClientCommandLegacy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE,
        properties = {"spring.shell.interactive.enabled=false"})
class AufgabeStadtwerkeApplicationTests {

    private String expectedActivity = "Mocked Activity";
    private String responseBody = "{\"activity\":\"Mocked Activity\"}";
    private ResponseEntity<String> responseEntity = new ResponseEntity<>(responseBody, HttpStatus.OK);





    @Autowired
    BoredApiClientCommandLegacy boredApiClientCommandLegacy;

    @Autowired
    BoredApiClientCommand boredApiClientCommand;

    @MockBean
    RestTemplate restTemplate;

    @Test
    public void testGetActivityByTypeLegacy() {
        String expectedUrl = "http://www.boredapi.com/api/activity?type=education";

        when(restTemplate.getForEntity(expectedUrl, String.class))
                .thenReturn(responseEntity);

        String activity = boredApiClientCommandLegacy.getActivityByType("education");

        assertEquals(expectedActivity, activity);
    }

    @Test
    public void testGetActivityLegacy() {
        String expectedUrl = "http://www.boredapi.com/api/activity";

        when(restTemplate.getForEntity(expectedUrl, String.class))
                .thenReturn(responseEntity);

        String activity = boredApiClientCommandLegacy.getActivityByType(null);

        assertEquals(expectedActivity, activity);
    }

    @Test
    public void testGetActivityByKey() {
        String expectedUrl = "http://www.boredapi.com/api/activity?key=1000000";
        when(restTemplate.getForEntity(expectedUrl, String.class))
                .thenReturn(responseEntity);


        String key = "1000000";

        String result = boredApiClientCommand.getActivity(null, null, null, null, key);

        assertEquals(expectedActivity, result);
    }

    @Test
    public void testGetActivity() {
        String expectedUrl = "http://www.boredapi.com/api/activity";
        when(restTemplate.getForEntity(expectedUrl, String.class))
                .thenReturn(responseEntity);

        String result = boredApiClientCommand.getActivity(null, null, null, null, null);
        assertEquals(expectedActivity, result);


    }

    @Test
    public void testGetActivityByMultipleOptions() {
        String expectedUrl = "http://www.boredapi.com/api/activity?type=education&participants=3&accessibility=0.1&price=0";
        when(restTemplate.getForEntity(expectedUrl, String.class))
                .thenReturn(responseEntity);

        String type = "education";
        String participants = "3";
        String accessibility = "0.1";
        String cost = "0";

        String result = boredApiClientCommand.getActivity(type, participants, accessibility, cost, null);
        assertEquals(expectedActivity, result);
    }
}

